#include "opOverClass.h"


opOverClass::opOverClass()
{
    a = 10;
    b = 10;
    c = new int(10);
    //ctor
}

opOverClass::opOverClass(int a_in, int b_in){
     a = a_in;
     b = b_in;
     c = new int(10);
}

const opOverClass&  opOverClass::operator=(const opOverClass& o_in){

   if(this != &o_in) {
      this->a = o_in.a;
      this->b = o_in.b;
      for (int i = 0; i < 10; i ++)
        this->c[i] = o_in.c[i];
   }
   return *this;
}

opOverClass opOverClass::operator+(opOverClass o_in){
   opOverClass temp;
   temp.a = this->a + o_in.a;
   temp.b = this->b + o_in.b;
   return temp;
}
#if OVERLOAD_AS_MEMBER
opOverClass opOverClass::operator*(opOverClass o_in){
   opOverClass temp;
   temp.a = this->a * o_in.a;
   temp.b = this->b * o_in.b;
   return temp;
}
#else
opOverClass operator*(opOverClass o_in1, opOverClass o_in2){
   opOverClass temp;
   temp.a = this->a * o_in.a;
   temp.b = this->b * o_in.b;
   return temp;
}
#endif
bool opOverClass::operator==(opOverClass o_in){
   if(this->a == o_in.a && this->b == o_in.b)
     return true;
   else
     return false;
}

int opOverClass::get_a(){
  return a;
}
int opOverClass::get_b(){
  return b;
}
opOverClass::~opOverClass()
{
    delete []c;
    //dtor
}
