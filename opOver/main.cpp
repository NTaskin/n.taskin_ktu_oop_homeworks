#include <iostream>
#include "opOverClass.h"
using namespace std;


istream& operator >>(istream& is, opOverClass& o_in){
   is >> o_in.a >> o_in.b;
   return is;
}

ostream& operator <<(ostream& os, const opOverClass& o_in){

   os << "(" << o_in.a << "," << o_in.b << ")" << endl;
   return os;
}

int main()
{
    opOverClass a(10,20);
    opOverClass b(20,30);
    opOverClass c = a + b;//c = a.operator+(b)
    opOverClass d = a * c;
    //cout << "Hello world! " << c.get_a() <<" " << c.get_b() << endl;
    //cout << "Hello world! " << d.get_a() <<" " << d.get_b() << endl;
    if (a == b) cout << "they are equal" << endl;
    else cout << "they are not equal" << endl;
    cout << d;
    cin >> d;
    cout << d;
    return 0;
}
